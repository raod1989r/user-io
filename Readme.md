Problem statement :  To pick out the best team from a list of candidates for a specified budget.

There is a database of candidates with the following attributes.
Name
Experience in years
expected Salary per month

The team configuration needed is
X  Senior Developers (3 -10 yrs experience)
Y Junior Developers. ( less then 3 years experience)

You need to write the code in php and use mongodb as database

Write script to build randomise data of about 5000 candidates.
Build a page in php / yii framework to accept the following parameters
number of senior developers
number of junior developers
Budget per month
The page should  do ajax queries and show me the best team possible.
Build a crud layout for candidates so as to be able to change salary and reflect  a new team.


Notes
---

To Start
--
npm install

gulp serve

