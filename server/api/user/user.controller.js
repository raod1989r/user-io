/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/users              ->  index
 * POST    /api/users              ->  create
 * GET     /api/users/:id          ->  show
 * PUT     /api/users/:id          ->  upsert
 * PATCH   /api/users/:id          ->  patch
 * DELETE  /api/users/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import User from './user.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      // eslint-disable-next-line prefer-reflect
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Users
export function index(req, res) {
  var seniorsLimit = parseInt(req.query.seniorsLimit);
  var juniorsLimit = parseInt(req.query.juniorsLimit);
  var budget = parseInt(req.query.budget);
  var searchBudget = 0;
  var seniorsTotalBudget = 0;
  var selectedSeniors = [];
  var selectedJuniors = [];
  var _this = this;

  User.aggregate([
    {
      $match: {
        experience: {$gte: 3},
        expected_salary: {$lte: budget}
      }
    },
    {
      $group: {
        _id: {
          expected_salary: '$expected_salary',
          name: '$name',
          experience: '$experience'
        }
      }
    },
      {$sort: {"_id.expected_salary": 1}},
      {$limit: seniorsLimit}
  ],
    function(err, seniors) {

      seniors.every(function(senior){
        var currentBudget = searchBudget + senior._id.expected_salary;

        if(currentBudget < budget) {
          selectedSeniors.push(senior);
          searchBudget = currentBudget;
          return true;
        }else{
          return false;
        }
      });
      seniorsTotalBudget = searchBudget;

      User.aggregate([
          {
            $match: {
              experience: {$lte: 3},
              expected_salary: {$lte: budget}
            }
          },
          {
            $group: {
              _id: {
                expected_salary: '$expected_salary',
                name: '$name',
                experience: '$experience'
              }
            }
          },
        {$sort: {"_id.expected_salary": 1}},
        {$limit: juniorsLimit}
        ], function (err, juniors) {

        juniors.every(function (junior) {
          var currentBudget = searchBudget + junior._id.expected_salary;

          if(currentBudget < budget) {
            selectedJuniors.push(junior);
            searchBudget = currentBudget;
            return true;
          }else{
            return false;
          }
        });

          res.status(200).json({
            juniors: selectedJuniors,
            seniors: selectedSeniors,
            seniorsTotalBudget: seniorsTotalBudget,
            juniorsTotalBudget: searchBudget - seniorsTotalBudget
          });
        }
      )
    })
}

// Gets a single User from the DB
export function show(req, res) {
  return User.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new User in the DB
export function create(req, res) {
  return User.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given User in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return User.findOneAndUpdate({_id: req.params.id}, req.body, {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing User in the DB
export function patch(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return User.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a User from the DB
export function destroy(req, res) {
  return User.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
