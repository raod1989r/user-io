'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './user.events';

var UserSchema = new mongoose.Schema({
  name: String,
  expected_salary: {type: Number},
  experience: {type: Number}
});

registerEvents(UserSchema);
export default mongoose.model('User', UserSchema);
