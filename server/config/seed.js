/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
import Thing from '../api/thing/thing.model';
import config from './environment/';
import User from '../api/user/user.model';
import faker from 'faker';

export default function seedDatabaseIfNeeded() {
  if(config.seedDB) {


    User.find({}).remove()
      .then(function() {

        function getRandomInt(min, max) {
          return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        for (var index = 0; index < 5000; index++) {
          var experience = getRandomInt(0, 30);

          User.create({
            name: faker.name.findName(),
            expected_salary: getRandomInt(20000, 300000),
            experience: experience
          });
        }

        console.log("Populated Users Data");
      })
  }
}
