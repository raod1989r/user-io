'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './user.routes';

export class UserComponent {
  /*@ngInject*/
  constructor($http) {
    this.budget = 240000;
    this.juniorsLimit = 20;
    this.seniorsLimit = 8;
    this.$http = $http;
    this.isLoading = false;
    this.requestServer = false;
    this.users = {'juniors': [], 'seniors': []};
  }

  getTeam() {
    var _this = this;

    this.isLoading = true;
    this.$http.get('/api/users?budget='+this.budget +'&juniorsLimit='+this.juniorsLimit + '&seniorsLimit=' + this.seniorsLimit)
      .then(function(response) {
        _this.users = response.data;
        _this.isLoading = false;
        _this.requestServer = true;
      })
  }
}

export default angular.module('userIoApp.user', [uiRouter])
  .config(routes)
  .component('user', {
    template: require('./user.html'),
    controller: UserComponent,
    controllerAs: 'userCtrl'
  })
  .name;
